const pool = require('../config/database')
const { v4: uuidv4 } = require('uuid')


module.exports = {
  insertTbUser: ({ name, birth_date }) => {
    return new Promise((resolve, reject) => {
      const query = `
      INSERT INTO tb_user(name, birth_date)
      VALUES ('${name}', '${birth_date}')
      RETURNING id;
      `

      pool.connect((err, result, release) => {
        if (err) return reject(err)

        pool.query(query, (err, result) => {
          release()
          if (err) return reject(err)

          return resolve(result)
        })
      })
    })
  },
  insertTbMessage: ({ user_id }) => {
    return new Promise((resolve, reject) => {
      const uuid = uuidv4()
      const query = `
      INSERT INTO tb_message(user_id, uuid)
      VALUES ('${user_id}', '${uuid}')
      RETURNING *;
      `

      pool.connect((err, result, release) => {
        if (err) return reject(err)

        pool.query(query, (err, result) => {
          release()
          if (err) return reject(err)

          return resolve(result)
        })
      })
    })
  },
  getAllMessage: () => {
    return new Promise((resolve, reject) => {
      const query = `
      SELECT 
        u.name,
        u.birth_date,
        m.uuid as message_id
      FROM
        tb_user u
      LEFT JOIN
        tb_message m ON m.user_id = u.id
      `
      pool.connect((err, result, release) => {
        if (err) return reject(err)

        pool.query(query, (err, result) => {
          release()
          if (err) return reject(err)

          return resolve(result)
        })
      })
    })
  },
  getOneMessage: ({ message_id }) => {
    return new Promise((resolve, reject) => {
      const query = `
      SELECT 
        u.name,
        u.birth_date,
        m.uuid as message_id
      FROM
        tb_user u
      LEFT JOIN
        tb_message m ON m.user_id = u.id
      WHERE
        m.uuid = '${message_id}'
      `
      pool.connect((err, result, release) => {
        if (err) return reject(err)

        pool.query(query, (err, result) => {
          release()
          if (err) return reject(err)

          return resolve(result)
        })
      })
    })
  },
  deleteMessage: ({ message_id }) => {
    return new Promise((resolve, reject) => {
      const query = `
      DELETE FROM tb_message WHERE uuid = '${message_id}' RETURNING user_id
      `
      pool.connect((err, result, release) => {
        if (err) return reject(err)

        pool.query(query, (err, result) => {
          release()
          if (err) return reject(err)

          return resolve(result)
        })
      })
    })
  },
  deleteTbUser: ({ user_id }) => {
    return new Promise((resolve, reject) => {
      const query = `
      DELETE FROM tb_user WHERE id = '${user_id}' RETURNING id
      `
      pool.connect((err, result, release) => {
        if (err) return reject(err)

        pool.query(query, (err, result) => {
          release()
          if (err) return reject(err)

          return resolve(result)
        })
      })
    })
  },
  truncateTableUser: () => {
    return new Promise((resolve, reject) => {
      const query = `
      TRUNCATE TABLE tb_user CASCADE;
      `
      pool.connect((err, result, release) => {
        if (err) return reject(err)

        pool.query(query, (err, result) => {
          release()
          if (err) return reject(err)

          return resolve(result)
        })
      })
    })
  }
}