const { getAllMessage, getOneMessage, deleteMessage, deleteTbUser } = require('../models')

module.exports = {
  getAllMessage: async (req, res, next) => {
    try {
      const result = await getAllMessage()
      res.status(200).json({
        data: result.rows
      })
    } catch (error) {
      res.status(500).json({
        error
      })
    }
  },
  getOneMessage: async (req, res, next) => {
    try {
      const message_id = req.params.id
      const result = await getOneMessage({ message_id })
      res.status(200).json({
        data: result.rows
      })
    } catch (error) {
      res.status(500).json({
        error
      })
    }
  },
  deleteMessage: async (req, res, next) => {
    try {
      const { message_id } = req.body
      const result = await deleteMessage({ message_id })
      const user_id = result.rows[0].user_id

      await deleteTbUser({ user_id })

      res.status(201).json({
        message: `Succes delete message with id: ${message_id}`
      })
    } catch (error) {
      res.status(500).json({
        error
      })
    }
  }
}