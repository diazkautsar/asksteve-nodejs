const schema = {
  properties: {
    name: {
      pattern: /^[a-zA-Z\s\-]+$/,
      message: 'Name must be only letters, spaces, or dashes',
      required: true,
      description: 'First Name'
    },
    birth_date: {
      pattern: /^((19|20)\d{2})\-(0[1-9]|1[0-2])\-(0[1-9]|1\d|2\d|3[01])$/,
      message: 'Invalid Format. Please Use This Pattern YYYY-MM-DD',
      required: true,
      description: 'Your Birth Date'
    },
    want_to_know_birthdate: {
      description: 'do you want to know how many days you have to go to your birthday?'
    }
  }
};

module.exports = schema