const prompt = require('prompt');
const schema = require('../../config/conversation')
const dictionary = require('../../assets/dictionary')
const { findDaysUnitlBirthday } = require('../../helpers')
const { insertTbUser, insertTbMessage } = require('../../models')
const pool = require('../../config/database')

prompt.start();

console.log('Hy!')
prompt.get(schema, async function (err, result) {
  if (err) {
    console.log(err)
  }

  const { rows } = await insertTbUser({ name: result.name, birth_date: result.birth_date })
  const id = rows[0].id

  await insertTbMessage({ user_id: id })

  pool.end()

  if (dictionary.agree.includes(result.want_to_know_birthdate)) {
    const days = findDaysUnitlBirthday(result.birth_date)

    if (days === 0) {
      console.log('Today Its Your Birthday. Happy Birthday. Wish You All The Best')
    } else {
      console.log('There are ' + days + ' days left until your next birthday')
    }
  } else if (dictionary.disagree.includes(result.want_to_know_birthdate)) {
    console.log('Goodbye')
  } else {
    console.log('Goodbye')
  }
});
