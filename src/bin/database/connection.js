const pool = require('../../config/database')

pool.connect((err, result, release) => {
  if (err) {
    return console.error('ERROR WHILE CONNECTED ', err.stack)
  }

  pool.query('SELECT NOW()', (err, result) => {
    release()
    if (err) {
      return console.error('ERROR WHILE CONNECTED ', err.stack)
    }
    return console.log('Database connected. Ready To Use')
  })
  pool.end()
})

