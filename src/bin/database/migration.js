const fs = require('fs')
const pool = require('../../config/database')

const files = fs.readdirSync('src/database/migration')

async function proccessMigrate () {
  const promises = files.map(item => {
    const query = require(`../../database/migration/${item}`)
    return query()
  })

  try {
    const result = await Promise.all(promises)
    result.forEach(item => console.log(item))
  } catch (error) {
    console.log(error)
  } finally {
    pool.end()
  }

}

proccessMigrate()