const pool = require('../../config/database')

const queryTbUser = `
CREATE TABLE IF NOT EXISTS tb_user(
  id SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL,
  birth_date VARCHAR NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMP NOT NULL DEFAULT NOW()
)`;

const createTbUser = async () => {
  return new Promise((resolve, reject) => {
    pool.query(queryTbUser, (err, result) => {
      if (err) {
        return reject(err)
      }
  
      return resolve('Migration Success. tb_user')
    })
  })
}


module.exports = createTbUser
