const pool = require('../../config/database')

const queryTbMessage = `
CREATE TABLE IF NOT EXISTS tb_message(
  id SERIAL PRIMARY KEY,
  uuid VARCHAR NOT NULL,
  user_id INT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
  FOREIGN KEY (user_id) REFERENCES tb_user(id) ON DELETE CASCADE ON UPDATE CASCADE
)`;

const createTbMessage = async () => {
  return new Promise((resolve, reject) => {
    pool.query(queryTbMessage, (err, result) => {
      if (err) {
        return reject(err)
      }
  
      return resolve('Migration Success. tb_message')
    })
  })
}


module.exports = createTbMessage
