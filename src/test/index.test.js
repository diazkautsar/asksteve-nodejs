const request = require('supertest')
const app = require('../app')
const { insertTbUser, insertTbMessage, truncateTableUser } = require('../models')

let first_message_id = null
let second_message_id = null

describe('ENDPOINT GET /message', () => {
  beforeEach((done) => {
    const payload = {
      name: 'DK',
      birth_date: '1995-09-07' 
    }
    insertTbUser(payload)
      .then(result => {
        const id = result.rows[0].id
        return insertTbMessage({ user_id: id })
      })
      .then(result => {
        first_message_id = result.rows[0].uuid
        done()
      })
      .catch(err => {
        done(err)
      })
  })

  beforeEach((done) => {
    const payload = {
      name: 'SELVI',
      birth_date: '1995-06-18' 
    }
    insertTbUser(payload)
      .then(result => {
        const id = result.rows[0].id
        return insertTbMessage({ user_id: id })
      })
      .then(result => {
        second_message_id = result.rows[0].uuid
        done()
      })
      .catch(err => {
        done(err)
      })
  })

  afterEach((done) => {
    truncateTableUser()
      .then(() => {
        done()
      })
      .catch(err => {
        console.error(err)
      })
  })

  test('GET /message. get all message available', (done) => {
    const expected = {
      data: [
        {
          name: 'DK',
          birth_date: '1995-09-07',
          message_id: first_message_id,
        },
        {
          name: 'SELVI',
          birth_date: '1995-06-18',
          message_id: second_message_id
        }
      ]
    }
    request(app)
      .get('/messages')
      .end((err, response) => {
        expect(err).toBe(null)
        expect(response.body).toEqual(expected)
        done()
      })
  })

  test('GET /messagse/:id. Get one message', (done) => {
    const expected = {
      data: [
        {
          name: 'DK',
          birth_date: '1995-09-07',
          message_id: first_message_id,
        },
      ]
    }
    request(app)
      .get(`/messages/${first_message_id}`)
      .end((err, response) => {
        expect(err).toBe(null)
        expect(response.body).toEqual(expected)
        done()
      })
  })

  test('Delete /messages. Delete message', (done) => {
    const expected = {
      message: `Succes delete message with id: ${second_message_id}`
    }
    request(app)
      .delete(`/messages`)
      .send({
        message_id: second_message_id
      })
      .end((err, response) => {
        expect(err).toBe(null)
        expect(response.body).toEqual(expected)
        done()
      })
  })
})