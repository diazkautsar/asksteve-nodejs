const router = require('express').Router()
const { getAllMessage, getOneMessage, deleteMessage } = require('../controllers')


router.get('/messages', getAllMessage)
router.get('/messages/:id', getOneMessage)
router.delete('/messages', deleteMessage)

module.exports = router