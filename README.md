# Ask Steve;s NodeJs - App

## Requirement

- Node.js `>= 12.0.0`
- ExpressJs `>= 4.17.0`
- PostgresSQL `12`

# Installation

1. Clone this repo to your localhost
2. Copy `.env.example` to `.env` and write your own env.
3. Install depedency
   ```bash
    npm install
   ```
4. Run the migration script

   ```bash
    npm run db-migrate
   ```

   > ⚠️ Before running migration script, make sure your database connection is connected
   > Run this script for check your database connection

   ```bash
   npm run db-init
   ```

   > If database connected. This command will print 'Database connected. Ready To Use'

5. You are ready to go

# Usage App

Start Conversation

```bash
npm run start-conversation
```

Run REST API

```bash
npm run dev
```

Run Tst

```bash
npm run test
```

# Rest API Docs

1. Get All Message

   ```bash
   method: 'Get',
   url: '/messages'
   ```

   Response success

   ```bash
    data: [
        {
            "name": "INTAN",
            "birth_date": "1995-10-20",
            "message_id": "3fe9932a-4efe-47ee-b305-eade2468a2ea"
        },
        {
            "name": "Djalal",
            "birth_date": "1995-07-09",
            "message_id": "46468413-9b79-43d8-b22b-4df79c02bce9"
        }
    ]
   ```

   Response Error

   ```bash
   error: 'error message'
   ```

2. Get one message

   ```bash
   method: 'Get',
   url: '/messages/:id'
   ```

   Response success

   ```bash
    data: [
        {
            "name": "Djalal",
            "birth_date": "1995-07-09",
            "message_id": "46468413-9b79-43d8-b22b-4df79c02bce9"
        }
    ]
   ```

   Response Error

   ```bash
   error: 'error message'
   ```

3. Delete message

   ```bash
   method: 'Delete',
   url: '/messages'
   body: {
       message_id: '46468413-9b79-43d8-b22b-4df79c02bce9'
   }
   ```

   Response success

   ```bash
    message: 'Succes delete message with id: 46468413-9b79-43d8-b22b-4df79c02bce9'
   ```

   Response Error

   ```bash
   error: 'error message'
   ```
